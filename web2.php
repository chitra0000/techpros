
<!DOCTYPE html>

<html lang="en">
    
  <head>
      
    <meta charset="utf-8">
      
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
      
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      
      <style>
      
          .jumbotron {
              
              <!--background-image: url(home.jpg);
	  -->
              text-align: center;
              margin-top: 50px;
			  
			   height:500px;
          }
          
          #email {
              
              width: 300px;
              
          }
          
          #appSummary {
              
              text-align: center;
              margin-top:50px;
              margin-bottom: 50px;
              
          }
          
          .card-img-top {
              
              width: 100%;
              
          }
          
          #appStoreIcon {
              
              width: 200px;
              
          }
		  
          #slide{
		  
			   background-size: cover;		
		  width:500px
		  height:100px;
		  interval:3000;}
		  
		  
          #footer {
              
              padding-top: 150px;
              margin-top: 50px;
              text-align: center;
              padding-bottom: 150px;
          }

         #children
          {
            
              margin-top: 100px;
              text-align: center;
             
          }
		  .c3{
		  color:grey;
		  }
          
          body {
              
              position: relative;
              
          }
		  .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
    background-color: #0275d8;
}
#aq1{
text-decoration:none;
}
.lightbox {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  
}
.toolbarLB {
  text-align: right;
  padding: 3px;
}
.t{
	color:#2F4F4F;
	text-align:center;
	
	
}
.closeLB {
  color: red;
  cursor: pointer;
}
.lightbox .iframeContainer {
  vertical-align: middle;
  background: #d3d3d3;
  opacity:0.9;
  box-shadow:2px 2px 8px rgba(0,0,0,0.5);
  padding: 2px;
}
.lightbox.closed {
  display: none;
}

      
      </style>
      
  </head>
    
  <body data-spy="scroll" data-target="#navbar" data-offset="500">
      
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Prime Minister’s National Relief Fund</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        In pursuance of an appeal by the then Prime Minister, Pt. Jawaharlal Nehru in January, 1948, the Prime Minister’s National Relief Fund (PMNRF) was established with public contributions to assist displaced persons from Pakistan. The resources of the PMNRF are now utilized primarily to render immediate relief to families of those killed in natural calamities like floods, cyclones and earthquakes, etc. and to the victims of the major accidents and riots. Assistance from PMNRF is also rendered, to partially defray the expenses for medical treatment like heart surgeries, kidney transplantation, cancer treatment and acid attack etc. The fund consists entirely of public contributions and does not get any budgetary support. The corpus of the fund is invested in various forms with scheduled commercial banks and other agencies. Disbursements are made with the approval of the Prime Minister. PMNRF has not been constituted by the Parliament. The fund is recognized as a Trust under the Income Tax Act and the same is managed by Prime Minister or multiple delegates for national causes. PMNRF operates from the Prime Minister’s Office, South Block, New Delhi-110011 and does not pays any license fee. PMNRF is exempt under Income Tax Act, 1961 under Section 10 and 139 for return purposes. Contributions towards PMNRF are notified for 100% deduction from taxable income under section 80(G) of the Income Tax Act, 1961. Prime Minister is the Chairman of PMNRF and is assisted by Officers/ Staff on honorary basis.

Permanent Account Number of PMNRF is XXXXXX637Q 
PMNRF accepts only voluntary donations by individuals and institutions.

Contributions flowing out of budgetary sources of Government or from the balance sheets of the public sector undertakings are not accepted. Conditional contributions, where the donor specifically mentions that the amount is meant for a particular purpose, are not accepted in the Fund.

In case the donor deposits donations directly in any of the PMNRF collection banks, they are advised to provide complete transaction details along with their address to this office through e-mail at pmnrf@gov.in for speedy issue of 80(G) Income Tax Receipts.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		 <input type="button" class="btn btn-info" value="Go to the Site"  onclick=" relocate8_home()">


<script>
function relocate8_home()
{
     location.href = "https://pmnrf.gov.in/en/";
} 
</script>
        
      </div>
    </div>
  </div>
</div>
      
        <nav class="navbar navbar-light bg-faded navbar-fixed-top" id="navbar">
          <a class="navbar-brand" href="#">DoNation</a>
          <ul class="nav navbar-nav">
            <!--<li class="nav-item">
              <a class="nav-link" href="#jumbotron">Home <span class="sr-only">(current)</span></a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link" href="#children">Children</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#women">Women</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#diffAbled">Differently-Abled</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#elderly">Elderly</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="#abandon">Abandoned Pets </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#footer">About Us</a>
.            </li>

            <li class="nav-item">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
 Prime Minister’s National Relief Fund
</button>

.            </li>

          </ul>
<form class="form-inline pull-xs-right">
            <!--<input class="form-control" type="email" placeholder="Email">
              <input class="form-control" type="password" placeholder="Password">-->
            <button class="btn btn-success" type="submit"><a id="aq1" href="new.php">Login</a></button>
          </form>
        </nav>
<div class="container-fluid" id="slide">
 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" id="theSlidelist">
    <div class="carousel-item active">
      <img class="d-block w-100" src="home.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img4.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img2.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div></div>
<script>
$( "#theSlidesList" ).find( ".item" ).css( "-webkit-transition", "transform 1.9s ease-in-out 0s" ).css( "transition", "transform 1.9s ease-in-out 0s" )
</script>
     <!-- <div class="jumbotron" id="jumbotron">
        
      
        </div>-->
		<div class="container" >
      
        <div class="row" id="appSummary" >
          
            <h1 >"DoNation": <i> Putting information in the hands of world!</i><br></h1><hr>
		
      
      
        <div class="container" ><div class="row" id="appSummary" >
          
            <h1 id="children">Children<br></h1>
            <p class="lead"><i>Children are great imitators.So give them something to imitate.</i></p>
          
          </div>
          
      </div>
      
      <div class="container" id="about">
      <div class="card-deck-wrapper">
  <div class="card-deck">
    <div class="card">
      <img class="card-img-top" src="child1.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-child" aria-hidden="true"></i></h4>
        <p class="card-text">Support the living expenses of HIV orphan<br><span class="c3">By Snehalaya</span></p>
       <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate1_home()">


<script>
function relocate1_home()
{
     location.href = "child2.html";
} 
</script>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="child2.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-child" aria-hidden="true"></i> </h4>
        <p class="card-text">Support a child suffering from cancer with monthly rations<br><span class="c3">By St Jude India Childcare Centres</span></p>
        <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate2_home()">


<script>
function relocate2_home()
{
     location.href = "child1.html";
} 
</script>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="img5.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-child" aria-hidden="true"></i> </h4>
        <p class="card-text">Sponser mid day meals to children in Orissa<br><span class="c3">By The Akshaya Patra Foundation</span></p>
       <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate2_home()">


<script>
function relocate2_home()
{
     location.href = "child1.html";
} 
</script>

      </div>
    </div>
  </div>
</div>
          </div> 
		  
		  

      
      <div class="container" >
      
        <div class="row" id="appSummary">
          
            <h1 id="women">Women</h1>
            <p class="lead"><i>She overcame everything that was meant to destroy her.</i></p>
          
          </div>
          
      </div>
      
      <div class="container" >
      <div class="card-deck-wrapper">
  <div class="card-deck">
    <div class="card">
      <img class="card-img-top" src="img6.jpg" alt="Card image cap">
      
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-female" aria-hidden="true"></i></i></h4>
        <p class="card-text">Help counselling help for domestic voilence help<br><span class="c3">By The Apanayalaya</span></p>
        <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate3_home()">


<script>
function relocate3_home()
{
     location.href = "women2.html";
} 
</script>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="img8.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-female" aria-hidden="true"></i></h4>
        <p class="card-text">Help women get access to sanitary pads<br><span class="c3">By Goonj</span></p>
       <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate4_home()">


<script>
function relocate4_home()
{
     location.href = "women1.html";
} 
</script>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="img9.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-female" aria-hidden="true"></i></h4>
        <p class="card-text">Help women of remote villages to get access to maternal help<br><span class="c3">By The Richa Foundation</span></p>
       <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate3_home()">


<script>
function relocate3_home()
{
     location.href = "women2.html";
} 
</script>
      </div>
    </div>
  </div>
</div>
          </div> 
		  
<div class="container" >
      
        <div class="row" id="appSummary" id="diffAbled">
          
            <h1 id="diffAbled" >Differently Abled</h1>
            <p class="lead"><i>Abled doesnot mean enabled.Disabled doesnot mean less abled.</i></p>
          
          </div>
          
      </div>
      
      <div class="container" >
      <div class="card-deck-wrapper">
  <div class="card-deck">
    <div class="card">
      <img class="card-img-top" src="img10.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-wheelchair" aria-hidden="true"></i> </h4>
        <p class="card-text">To sponser education for handicapped people.<br><span class="c3">By Jeevan Foundation</span></p>
        <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate2_home()">


<script>
function relocate2_home()
{
     location.href = "child1.html";
} 
</script>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="img11.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-wheelchair" aria-hidden="true"></i> </h4>
        <p class="card-text">To sponser funds for basic living needs of disabled people.<br><span class="c3">By Lata Foundation</span></p>
        <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate3_home()">


<script>
function relocate3_home()
{
     location.href = "women2.html";
} 
</script>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="img12.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-wheelchair" aria-hidden="true"></i></h4>
        <p class="card-text">Sponser the living expenses of people with mental disorders.<br><span class="c3">By Tarika Foundation</span></p>
       <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate2_home()">


<script>
function relocate2_home()
{
     location.href = "child1.html";
} 
</script>
      </div>
    </div>
  </div>
</div>
          </div> 
		  <div class="container" id="diffAbled">
      
        <div class="row" id="appSummary">
          
            <h1 id="elderly">Elderly</h1>
            <p class="lead"><i>To care for those who once cared for us is one of the highest honors.</i></p>
          
          </div>
          
      </div>
      
      <div class="container" >
      <div class="card-deck-wrapper">
  <div class="card-deck">
    <div class="card">
      <img class="card-img-top" src="img13.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-male" aria-hidden="true"></i></h4>
        <p class="card-text">Sponser the food and needs for elderly needy poor people.<br><span class="c3">By Kamala Foundation</span></p>
        <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate2_home()">


<script>
function relocate2_home()
{
     location.href = "child1.html";
} 
</script>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="img15.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-male" aria-hidden="true"></i></h4>
        <p class="card-text">To sponser basic clothing to the elderly<br><span class="c3">By Bimla Foundation</span></p>
        <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate2_home()">


<script>
function relocate2_home()
{
     location.href = "child1.html";
} 
</script>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="img14.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-male" aria-hidden="true"></i> </h4>
        <p class="card-text">Sponser funds for oldage homes<br><span class="c3">By Veera Foundation</span></p>
       <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate2_home()">


<script>
function relocate2_home()
{
     location.href = "child1.html";
} 
</script>
      </div>
    </div>
  </div>
</div>
          </div> 
		  <div class="container" id="diffAbled">
      
        <div class="row" id="appSummary">
          
            <h1 id="abandon">Abandoned Pets</h1>
            <p class="lead"><i>Donot let down an animal asking you for help.Maybe you are his last hope.</i></p>
          <input type="button" class="btn btn-info" value="Donate"  onclick=" relocate2_home()">


<script>
function relocate2_home()
{
     location.href = "child1.html";
} 
</script>
          </div>
          
      </div>
      
      <div class="container" >
      <div class="card-deck-wrapper">
  <div class="card-deck">
    <div class="card">
      <img class="card-img-top" src="img7.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-paw" aria-hidden="true"></i> </h4>
        <p class="card-text">To give basic shelter to abandoned animals<br><span class="c3">By Ananda Foundation</span></p>
        <a href="#" class="btn btn-primary stretched-link"> Donate</a>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="img17.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-paw" aria-hidden="true"></i></h4>
        <p class="card-text">To sponser the health care for needy animals.<br><span class="c3">By Petfeed Foundation</span></p>
        <a href="#" class="btn btn-primary stretched-link"> Donate</a>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="img18.jpg" alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title"><i class="fa fa-paw" aria-hidden="true"></i></h4>
        <p class="card-text">To sponser the basic needs of abandoned animals.<br><span class="c3">By Pawfect Foundation</span></p>
       <a href="#" class="btn btn-primary stretched-link"> Donate</a>
      </div>
    </div>
  </div>
</div>
          </div> 
		  
      
      <div id="footer">
      
        <div class="row">
          
                <h2>About Us</h2>
            
            <p class="lead">
			<i>
			The main moto of our "DoNation" is to help connect individuals and institutions who are willing to extend their support to the one's who are in need efficiently and easily.
			   </i>
			
			                       <i text-align="center">"Where there is charity and wisdom,There is neither fear nor ignorance. Every good act is charity"</i>
								   
								
			
			
			
			
			</b>
			<a class="btn btn-block btn-social btn-twitter">
    <span class="fa fa-twitter"></span> Sign in with Twitter
  </a>

			
			
			</p>
            
            <p><a href=""><img id="appStoreIcon" src="App-Store-Icon.png"></a></p>
            
            
          
          </div>
      
      </div>
	  <script>
    lightBoxClose = function() {
  document.querySelector(".lightbox").classList.add("closed");
}
</script>
<!-- the actual interesting bit -->
<div class="lightbox">
  <div class="iframeContainer">
    <div class="toolbarLB">
      <span class="closeLB" onclick="lightBoxClose()">x</span>
    </div>
	<div class="t">
	<h1><i>Good News!!!</i></h1>
	<h3id="j"> &nbsp; &nbsp;Do you realise that by donating you are doing a great work??<br>&nbsp; &nbsp;<br>
	 So here at DoNation you get an opportunity to help the needy people by:<br>
	-->You will be glad to know that, we will be donating from our side if you donate more than Rs.10,000!!<br>
	-->Giving more amount from you people will help them receiving more gifts and inspirations sessions from us<br>
	
	</h3>
	</div>
    <iframe width="450" height="350" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
    allowTransparency="true" style="z-index:99999px;"><button name="subject" type="submit">clickme</button></iframe>
  </div>
</div>
      
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
      
  </body>
    
</html>